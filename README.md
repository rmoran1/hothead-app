### Hothead App

5/8/2018
Ralph Moran and Elijah Hager

Contributions (50:50):

Elijah: Pandas dataframe, node size manipulation, presentation and logo design
Ralph: Tweepy (Twitter API), plotting and benchmarking, presentation

We both worked equally on this project and worked side-by-side most of the time.

##### Instructions for use:

1. "pip install" all of the imported module if you have not installed them already (including jupyter).

Also, run "pip install appmode" to install the GUI.

Launch the Jupyter notebook and click "Appmode" in the header.

Use the app as instructed.


#### Running Tests

To run tests, alter the line where the analyze() function is called, by putting either %timeit or %memit in front of it (separated by a space). Do not do both at once. Ensure that you have pip installed the memit module.
